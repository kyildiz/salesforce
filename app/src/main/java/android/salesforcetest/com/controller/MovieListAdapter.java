package android.salesforcetest.com.controller;

import android.content.Context;
import android.database.Cursor;
import android.salesforcetest.com.common.MovieItem;
import android.salesforcetest.com.omdb.R;
import android.salesforcetest.com.utils.DBUtils;
import android.salesforcetest.com.utils.ToastHelper;
import android.salesforcetest.com.view.FragmentMovieList;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Kemal on 3/25/2016.
 */
public class MovieListAdapter extends CursorAdapter {


    private List<MovieItem> movies = new ArrayList<MovieItem>();
    LayoutInflater inflater;
    private Context cxt;


    public MovieListAdapter(Context _context, List<MovieItem> employees) {
        super(_context, null, 0);
        this.movies = employees;
        cxt = _context;
        inflater = LayoutInflater.from(cxt);

    }

    public MovieListAdapter(Context context, Cursor c, int flags) {

        super(context, c, flags);
        cxt = context;

    }


    public void setMovies(List<MovieItem> data) {
        if (data == null || (data.size() == 1 && TextUtils.isEmpty(data.get(0).getTitle()))) {
            ToastHelper.movieNotFound(cxt);
        } else {
            movies = new ArrayList<MovieItem>();
            movies.addAll(data);
            notifyDataSetChanged();
        }
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(getRowLayoutID(), parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        MovieItem movieItem = new MovieItem();
        movieItem.setTitle(cursor.getString(FragmentMovieList.COL_TITLE));
        movieItem.setYear(cursor.getString(FragmentMovieList.COL_YEAR));
        movieItem.setDirector(cursor.getString(FragmentMovieList.COL_DIRECTOR));
        movieItem.setPlot(cursor.getString(FragmentMovieList.COL_PLOT));
        movieItem.setPoster(cursor.getString(FragmentMovieList.COL_POSTER));
        movieItem.setIsFavorite(true);
        bind(viewHolder, movieItem);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getCursor() == null)

        {
            final MovieItem movie = getItem(position);
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_movie, parent, false);
                viewHolder = new ViewHolder(convertView);


            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            viewHolder.txt_title.setText(movie.getTitle());
            viewHolder.txt_year.setText(movie.getYear());
            viewHolder.txt_plot.setText(movie.getPlot());
            viewHolder.txt_director.setText(movie.getDirector());
            Picasso.with(cxt).load(movie.getPoster()).placeholder(R.mipmap.ic_launcher).into(viewHolder.img_poster);

            if(movie.isFavorite())
                viewHolder.img_fav.setImageResource(R.drawable.removefavorite);
            else
                viewHolder.img_fav.setImageResource(R.drawable.addfavorite);
            View.OnClickListener buttonListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (movie.isFavorite()) {
                        viewHolder.img_fav.setImageResource(R.drawable.addfavorite);

                    } else {
                        viewHolder.img_fav.setImageResource(R.drawable.removefavorite);

                    }

                    movie.setIsFavorite(!movie.isFavorite());;
                    addRemoveFavorite(movie.isFavorite(), movie);

                }
            };

            viewHolder.img_fav.setOnClickListener(buttonListener);


            convertView.setTag(viewHolder);

            return convertView;
        } else
            return super.getView(position, convertView, parent);
    }

    /**
     * Cache of the children views for a movie list item.
     */
    public static class ViewHolder {

        public final TextView txt_title;
        public final TextView txt_director;
        public final TextView txt_year;
        public final TextView txt_plot;
        public final ImageView img_poster;
        public final ImageButton img_fav;

        public ViewHolder(View view) {
            txt_title = (TextView) view.findViewById(R.id.txt_title);
            txt_director = (TextView) view.findViewById(R.id.txt_director);
            txt_year = (TextView) view.findViewById(R.id.txt_year);
            txt_plot = (TextView) view.findViewById(R.id.txt_plot);
            img_poster = (ImageView) view.findViewById(R.id.img_poster);
            img_fav = (ImageButton) view.findViewById(R.id.img_fav);
        }
    }


    @Override
    public MovieItem getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        if (getCursor() != null)
            return super.getCount();
        return movies.size();
    }


    private void addRemoveFavorite(boolean isFavorite, MovieItem movie) {
        if (isFavorite)
            DBUtils.insertMovie(cxt, movie);
        else
            DBUtils.deleteMovie(cxt, movie);
    }


    public void bind(final ViewHolder viewHolder, final MovieItem movieItem) {
        viewHolder.txt_title.setText(movieItem.getTitle());
        viewHolder.txt_year.setText(movieItem.getYear());
        viewHolder.txt_plot.setText(movieItem.getPlot());
        viewHolder.txt_director.setText(movieItem.getDirector());

        if(movieItem.isFavorite())
            viewHolder.img_fav.setImageResource(R.drawable.removefavorite);

        Picasso.with(cxt).load(movieItem.getPoster()).placeholder(R.mipmap.ic_launcher).into(viewHolder.img_poster);


        View.OnClickListener buttonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (movieItem.isFavorite()) {
                    viewHolder.img_fav.setImageResource(R.drawable.addfavorite);

                } else {
                    viewHolder.img_fav.setImageResource(R.drawable.removefavorite);

                }

                movieItem.setIsFavorite(!movieItem.isFavorite());
                addRemoveFavorite(movieItem.isFavorite(), movieItem);

            }
        };

        viewHolder.img_fav.setOnClickListener(buttonListener);

    }

    /**
     * Get Layout ID for this adapter
     *
     * @return
     */
    private static int getRowLayoutID() {
        return R.layout.list_item_movie;
    }


}
