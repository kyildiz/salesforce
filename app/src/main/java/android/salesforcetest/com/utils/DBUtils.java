package android.salesforcetest.com.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.salesforcetest.com.common.MovieItem;
import android.salesforcetest.com.data.MovieContract;
import android.util.Log;

/**
 * Created by Kemal on 3/26/2016.
 */
public class DBUtils {

    private static final String TAG = "DBUtils";
    public static void insertMovie(Context context, MovieItem item)
    {
        Log.d(TAG, "insertMovie: ");
        ContentValues movieValues = new ContentValues();
        movieValues.put(MovieContract.MovieEntry.COLUMN_TITLE, item.getTitle());
        movieValues.put(MovieContract.MovieEntry.COLUMN_DIRECTOR, item.getDirector());
        movieValues.put(MovieContract.MovieEntry.COLUMN_PLOT, item.getPlot());
        movieValues.put(MovieContract.MovieEntry.COLUMN_YEAR, item.getYear());
        movieValues.put(MovieContract.MovieEntry.COLUMN_POSTER, item.getPoster());
        context.getContentResolver().insert(MovieContract.MovieEntry.CONTENT_URI, movieValues);
    }

    public static void deleteMovie(Context context, MovieItem item)
    {
        Log.d(TAG, "deleteMovie: ");
        String where = MovieContract.MovieEntry.COLUMN_TITLE + "=? AND " + MovieContract.MovieEntry.COLUMN_DIRECTOR  +"=? AND " +
                MovieContract.MovieEntry.COLUMN_PLOT + "=? AND " + MovieContract.MovieEntry.COLUMN_YEAR  +"=? AND " +
                MovieContract.MovieEntry.COLUMN_POSTER + "=?";
        String[] args = {item.getTitle(), item.getDirector(), item.getPlot(), item.getYear(), item.getPoster()};
        context.getContentResolver().delete(MovieContract.MovieEntry.CONTENT_URI, where, args);
    }


    public static boolean checkFavorites(Context ctx, MovieItem movieItem) {
        Log.d(TAG, "checkFavorites: ");
        String[] project = {MovieContract.MovieEntry.COLUMN_TITLE};
        String where = MovieContract.MovieEntry.COLUMN_TITLE + "=? AND " + MovieContract.MovieEntry.COLUMN_YEAR + "=?";
        String[] args = {movieItem.getTitle(), movieItem.getYear()};
        //Check to see if movie is in favorites
        Cursor cursor = ctx.getContentResolver().query(MovieContract.MovieEntry.CONTENT_URI, project, where, args, null);

        if (cursor.getCount() > 0) {
            cursor.close();
            return true;
        }
        else
        {
            cursor.close();
            return false;
        }
    }
}
