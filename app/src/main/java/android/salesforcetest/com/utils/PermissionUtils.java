package android.salesforcetest.com.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.jar.Manifest;

/**
 * Created by Kemal on 3/26/2016.
 */
public class PermissionUtils {

    // Check 6.0 and above run time permission check for internet
    // We can provide explanation and listen for result but for this sample app i am passing those conditions
    public static boolean checkforInternetPermission(Activity activity)
    {
        if(Integer.valueOf(android.os.Build.VERSION.SDK_INT)<23) {
            return true;
        }
        else {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(activity,
                    android.Manifest.permission.INTERNET)
                    != PackageManager.PERMISSION_GRANTED) {


                    ActivityCompat.requestPermissions(activity,
                            new String[]{android.Manifest.permission.INTERNET}, 1);
                return  false;
            }
            else
                return  true;

        }


    }
}
