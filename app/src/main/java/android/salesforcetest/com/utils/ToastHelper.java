package android.salesforcetest.com.utils;

import android.app.Activity;
import android.content.Context;
import android.salesforcetest.com.omdb.R;
import android.widget.Toast;

/**
 * Created by Kemal on 3/26/2016.
 */
public class ToastHelper {
    
    public static void movieNotFound(Context context)
    {
        Toast.makeText(context, R.string.movie_not_found,
                Toast.LENGTH_LONG).show();
    }

    public static void connectionError(Activity activity)
    {
        Toast.makeText(activity, R.string.connection_error,
                Toast.LENGTH_LONG).show();
    }



}
