package android.salesforcetest.com.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;

/**
 * Created by Kemal on 3/26/2016.
 */
public class NetworkUtils {

    public static boolean isNetworkAvailable(Fragment fragment) {
        if(PermissionUtils.checkforInternetPermission(fragment.getActivity())) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) fragment.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if( activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                return true;
            }
            else
            {
                ToastHelper.connectionError(fragment.getActivity());
                return false;
            }

        }
        return false;
    }
}
