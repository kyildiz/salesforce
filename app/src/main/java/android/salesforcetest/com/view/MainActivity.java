package android.salesforcetest.com.view;

import android.app.Activity;
import android.salesforcetest.com.omdb.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Created by Kemal on 3/25/2016.
 */

public class MainActivity extends AppCompatActivity {



    private boolean mTwoPane;
    public static final String FAVORITEFRAGMENT_TAG = "FAVORITETAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.fragment_movie_favorites_list) != null) {
            // The detail container view will be present only in the large-screen layouts
            // (res/layout-sw600dp). If this view is present, then the activity should be
            // in two-pane mode.
            mTwoPane = true;
            // In two-pane mode, show the favorites view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            if (savedInstanceState == null) {

                FragmentMovieList favs = new FragmentMovieList();
                Bundle bundle = new Bundle();
                bundle.putBoolean(getResources().getString(R.string.favorite), true);
                favs.setArguments(bundle);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_movie_favorites_list, favs, FAVORITEFRAGMENT_TAG)
                        .commit();
            }
        } else {
            mTwoPane = false;
            getSupportActionBar().setElevation(0f);
        }


    }






}
