package android.salesforcetest.com.view.support;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;


/**
 * Created by Kemal on 3/25/2016.
 */

public class SegmentedRadioGroup extends RadioGroup {

	public SegmentedRadioGroup(Context context) {
		super(context);
	}
	
	public SegmentedRadioGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
	}
	
}