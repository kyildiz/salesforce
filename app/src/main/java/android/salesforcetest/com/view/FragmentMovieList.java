package android.salesforcetest.com.view;


import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.salesforcetest.com.common.MovieItem;
import android.salesforcetest.com.controller.MovieListAdapter;
import android.salesforcetest.com.data.MovieContract;
import android.salesforcetest.com.omdb.R;
import android.salesforcetest.com.utils.NetworkUtils;
import android.salesforcetest.com.view.support.SegmentedRadioGroup;
import android.salesforcetest.com.worker.MovieLoader;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kemal on 3/25/2016.
 */
public class FragmentMovieList extends Fragment implements LoaderManager.LoaderCallbacks {

    private boolean isFavorite;
    private RadioButton btnSearch;
    private RadioButton btnFavorites;
    private FrameLayout frameSearch;
    private ImageButton imgBtnSearch;
    private EditText txtSearch;
    private ListView listMovie;
    private MovieListAdapter movieAdapter;
    private int fragmentID;
    private int movie_list_loaderID = 1;
    private int movie_list_favorites_loaderID = 2;
    private int loaderID = movie_list_loaderID;


    private static final String[] MOVIE_COLUMNS = {
            MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID,
            MovieContract.MovieEntry.COLUMN_TITLE, MovieContract.MovieEntry.COLUMN_YEAR,
            MovieContract.MovieEntry.COLUMN_DIRECTOR, MovieContract.MovieEntry.COLUMN_PLOT,
            MovieContract.MovieEntry.COLUMN_POSTER

    };

    public static final int COL_ID = 0;
    public static final int COL_TITLE = 1;
    public static final int COL_YEAR = 2;
    public static final int COL_DIRECTOR = 3;
    public static final int COL_PLOT = 4;
    public static final int COL_POSTER = 5;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_movie, container, false);
        if (getArguments() != null) {
            fragmentID = R.id.fragment_movie_favorites_list;
            loaderID = movie_list_favorites_loaderID;
            isFavorite = getArguments().getBoolean(getResources().getString(R.string.favorite));
        } else {
            fragmentID = R.id.fragment_movie_list;
            loaderID = movie_list_loaderID;
            isFavorite = false;
        }


        intUI(rootView, savedInstanceState);


        return rootView;
    }

    private void intUI(View rootView, Bundle savedInstanceState) {

        SegmentedRadioGroup segmentGroup = (SegmentedRadioGroup) rootView.findViewById(R.id.segmentGroup);
        frameSearch = (FrameLayout) rootView.findViewById(R.id.frameSearch);
        imgBtnSearch = (ImageButton) frameSearch.findViewById(R.id.imgBtnSearch);
        txtSearch = (EditText) frameSearch.findViewById(R.id.txtSearch);
        listMovie = (ListView) rootView.findViewById(R.id.listMovie);


        imgBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(txtSearch.getText().toString())) {

                    //Check for internet and internet permissions for 6.0 and above
                    if (NetworkUtils.isNetworkAvailable(getFragmentManager().findFragmentById(fragmentID))) {
                        Bundle arg = new Bundle();
                        arg.putString(getResources().getString(R.string.search), txtSearch.getText().toString());
                        getLoaderManager().restartLoader(loaderID, arg, (FragmentMovieList) getFragmentManager().findFragmentById(fragmentID)).forceLoad();

                    }


                }
            }

        });

        //Tablets do not show search/favorite tabs
        if (getResources().getBoolean(R.bool.isTablet)) {
            if(fragmentID == R.id.fragment_movie_favorites_list)
                switchSearchVisibility(false);
            segmentGroup.setVisibility(View.GONE);
            setAdapter();
        } else {
            segmentGroup.setVisibility(View.VISIBLE);
            btnSearch = (RadioButton) segmentGroup.findViewById(R.id.btnSearch);
            btnFavorites = (RadioButton) segmentGroup.findViewById(R.id.btnFavorites);
            if (btnSearch.isChecked())
                ReloadList(R.id.btnSearch);
            else
                ReloadList(R.id.btnFavorites);

            segmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    int id = group.getCheckedRadioButtonId();


                    ReloadList(id);

                }
            });

        }
            //get reference to views









    }

    private void setAdapter() {
        if (loaderID == movie_list_loaderID) {
            //set adapter search
            movieAdapter = new MovieListAdapter(getContext(), new ArrayList<MovieItem>());

        }
        //set adapter fav
        else {
            movieAdapter = new MovieListAdapter(getActivity(), null, 0);
        }

        listMovie.setAdapter(movieAdapter);
        if(loaderID == movie_list_loaderID)
            getLoaderManager().destroyLoader(movie_list_favorites_loaderID);
        else
            getLoaderManager().destroyLoader(movie_list_loaderID);
        getLoaderManager().initLoader(loaderID, null, this);

    }

    private void ReloadList(int id) {
        switch (id) {
            case R.id.btnSearch:

                loaderID = movie_list_loaderID;
                switchSearchVisibility(true);
                setAdapter();
                break;

            case R.id.btnFavorites:
                loaderID = movie_list_favorites_loaderID;
                setAdapter();
                switchSearchVisibility(false);


                break;
        }


    }



    //search box is not visible on favorites
    private void switchSearchVisibility(boolean vis) {
        if (vis)
            frameSearch.setVisibility(View.VISIBLE);
        else
            frameSearch.setVisibility(View.GONE);
    }

    //Cancel search if there is one ongoing
    private void cancelSearch() {
        getLoaderManager().getLoader(loaderID).cancelLoad();
    }


    @Override
    public Loader onCreateLoader(int id, Bundle args) {

        //search fragment
        if (loaderID == movie_list_loaderID){
            String search = "";
            if (args != null)
                search = args.getString(getResources().getString(R.string.search));
            return new MovieLoader(getContext(), search);
        }
        //favorites fragment
        else {
            Uri userListUri = MovieContract.MovieEntry.CONTENT_URI;


            return new CursorLoader(getActivity(),
                    userListUri,
                    MOVIE_COLUMNS,
                    null,
                    null,
                    null);
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        //search fragment
        if (loaderID == movie_list_loaderID) {
            movieAdapter.setMovies((List<MovieItem>) data);
            //favorites fragment
        } else {
            movieAdapter.swapCursor((Cursor) data);
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        //search fragment
        if (loaderID == movie_list_loaderID) {
            movieAdapter.setMovies(new ArrayList<MovieItem>());
            //favorites fragment
        } else {
            movieAdapter.swapCursor(null);
        }

    }


}
