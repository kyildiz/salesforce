package android.salesforcetest.com.worker;

import android.content.Context;
import android.net.Uri;
import android.salesforcetest.com.common.MovieItem;
import android.salesforcetest.com.data.MovieContract;
import android.salesforcetest.com.utils.DBUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kemal on 3/26/2016.
 */
public class MovieLoader extends android.support.v4.content.AsyncTaskLoader<Object> {


    private String search;
    private String LOG_TAG = "MovieLoader";
    private Context ctx;
    public MovieLoader(Context context) {
        super(context);
        ctx = context;
    }

    public MovieLoader(Context context, String search) {
        super(context);
        this.search = search;
        ctx = context;
    }

    @Override
    public List<MovieItem> loadInBackground() {
        // This method is called on a background thread and should generate a
        // new set of data to be delivered back to the client.
        List<MovieItem> data = new ArrayList<MovieItem>();

        MovieItem movieItem = getMovieJSON();
        if(DBUtils.checkFavorites(ctx, movieItem))
            movieItem.setIsFavorite(true);
        else
            movieItem.setIsFavorite(false);


        data.add(movieItem);

        return data;


    }

    private MovieItem getMovieJSON()
    {
        Log.d(LOG_TAG, "Starting sync");
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String movieListJsonStr = null;


        try {

            final String OMDPAPI_BASE_URL =
                    "http://www.omdbapi.com/?plot=short&r=json";

            final String TITLESEARCH = "t";

            Uri builtUri = Uri.parse(OMDPAPI_BASE_URL).buildUpon()
                    .appendQueryParameter(TITLESEARCH, search)
                    .build();

            URL url = new URL(builtUri.toString());

            // Create the request to slack, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            movieListJsonStr = buffer.toString();


        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the movie data, there's no point in attempting
            // to parse it.
        }

        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        if(movieListJsonStr != null) {
            Gson gson = new Gson();
            return gson.fromJson(movieListJsonStr, MovieItem.class);
        }
        else
            return null;

    }





    @Override
    protected void onStopLoading() {
        // The Loader is in a stopped state, so we should attempt to cancel the
        // current load (if there is one).
        cancelLoad();

    }


}
