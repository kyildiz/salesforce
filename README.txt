UI
App UI consist of 2 fragments and one activity
App uses 2 pane tables Search and Favourites are visible in the same time
Tabs on mobile devices used to change between search and favourites fragments

Loaders
MovieLoader uses AsyncTaskLoader to load data from web service also checks if the searched movie is in local database to mark it favourite
CursorLoader used to pass the data from content provider to adapter

Adapter
We are using one Cursor adapter (MovieListAdapter) to handle both list and cursor data

Database
One basic table uses ContentProvider 
  
Permissions/Network
I implemented checks for network status as well as API 23> support for on demand permissions
